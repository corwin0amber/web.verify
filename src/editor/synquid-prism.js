synquid = {
  'keyword': /\b(qualifier|data|termination|measure|match|if|then|else|where|with|in)\b/,
  'atom': /\b_v\b|\?\?/,
  'special': /\b(Int|Bool)\b/,
  'operator': /\\|--?|\+\+?|!=?=?|<=?|>=?|==?=?|&&?|\|\|?|\?|\*|\/|~|\^|%/,
  'bracket': /[}{|]/,
}

Prism.languages.synquid = {
  'error': [/Cannot find sufficiently strong refinements/],
  'messages': [
    {
      pattern: /\bCannot match shape\b[\S\s]*?\bwith shape\b/,
      inside: {
        'error': [/^Cannot match shape/, /with shape$/],
        'rest': synquid
      }
    },
    {
      pattern: /\bwhen checking\b[\S\s]*?\bin\b/,
      inside: {
        'error': [/^when checking/, /in$/],
        'rest': synquid
      }
    }],
  'rest': synquid
}
