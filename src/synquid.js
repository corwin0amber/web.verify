var fs = require('fs');
var child_process = require('child_process');

var projdir = process.cwd() + "/src"; //fs.realpathSync(window.location.pathname + "/..");
var filename = projdir + "/data/test.sq";
var programText = localStorage['synquid.editor.text'] || fs.readFileSync(filename, 'utf-8');

TOOLS = {
  synquid: function(filename) { 
    return {command: "synquid", args: [filename]};
  }
}

var FONT_SIZE = {
  default: 12,
  min: 4,
  step: 2
}
  
function run(filename) {
  var info = TOOLS.synquid(filename);
  var args = $('#exec #arguments').val().split().filter(function(x) { return x; });
  $(".output").text("...");
  $('#exec #run').attr('mode', '◼︎');
  var tool = child_process.spawn(info.command, info.args.concat(args));
  var terminate = function() { tool.kill(); };
  buffer = "";
  function puts(text) {
    buffer += text;
    $(".output").text(filterEscape(buffer));
    Prism.highlightElement($('.output')[0]);
  }
  tool.stdout.on('data', function(data) {
    puts(data.toString("utf-8"));
  });
  tool.stderr.on('data', function(data) {
    puts(data.toString("utf-8"));
  });
  tool.on('close', function(code, signal) {
    console.log(`exited with rc=${code} signal=${signal}`);
    if (signal) $(".output").text("Aborted.");
    $('#exec #run').attr('mode', '▶︎');
    $(window).off('terminate', terminate);
  });
  $(window).on('terminate', terminate);
}

function filterEscape(text) {
  return text.replace(/\x1b\[\d+m/g, '');
}

function createEditor(programText) {
  // - create CodeMirror
  var editor = CodeMirror(document.getElementById('editor'), {
    lineNumbers: true,
    matchBrackets: true,
    mode: "text/x-synquid",
    theme: "comcom"
  });
  editor.setValue(programText);
  return editor;
}

writeTempFileSync = function() {
  var info = null;
  
  function writeTempFileSync(content, suffix) {
    if (info === null) {
      var temp = require('temp').track();
      info = temp.openSync({suffix});
      fs.writeSync(info.fd, editor.getValue());
      fs.closeSync(info.fd);
      window.addEventListener('unload', function() { temp.cleanup(); });
    }
    else {
      // reuse the file
      fs.writeFileSync(info.path, content);
    }
    return info;
  }
  return writeTempFileSync;
}();


$(function() {
  var editor = createEditor(programText);
  window.editor = editor;

  $('#editor').resizable({ grid: [10000, 1] });  // this is so you can only resize vertically

  $('.output').addClass("language-synquid");

  function save(filename) {
    fs.writeFileSync(filename, editor.getValue());
  }
  
  function saveTemp(suffix) {
    return writeTempFileSync(editor.getValue(), suffix).path;
  }
  
  function go() {
    ext = path.extname(filename);
    run(saveTemp(ext));
  }
  
  $('#exec button#run').click(function(ev) { 
    if ($(ev.target).attr('mode') == '▶︎') go(); 
    else $(window).trigger('terminate');
  });

  var fontSize = FONT_SIZE.default;
  
  function changeFontSizeBy(delta) {
    changeFontSize(Math.max(fontSize + delta, FONT_SIZE.min));
  }
  
  function changeFontSize(size) {
    fontSize = size;
    $('body').css('font-size', `${fontSize}px`);
    editor.refresh();
  }

  function zoomIn()      { changeFontSizeBy(+FONT_SIZE.step); }
  function zoomOut()     { changeFontSizeBy(-FONT_SIZE.step); }
  function zoomDefault() { changeFontSize(FONT_SIZE.default); }
  
  $(window).keydown(function(event) {
    if ((event.which == 187) && (event.metaKey || event.ctrlKey)) {  // Ctrl+'+'
      zoomIn();
    }
    else if ((event.which == 189) && (event.metaKey || event.ctrlKey)) {  // Ctrl+'-'
      zoomOut();
    }
    else if ((event.which == 48) && (event.metaKey || event.ctrlKey)) {   // Ctrl+0
      zoomDefault();
    }
    else if (((event.which == 83 || event.which == 115 || event.which == 13) && 
              (event.metaKey || event.ctrlKey)) || (event.which == 19)) {  // Ctrl+S or Ctrl+Enter
      go();
      event.preventDefault();
      return false;
    }
    else return true;
  });
  
  //$(window).unload(function() { localStorage['synquid.editor.text'] = editor.getValue(); });
});

// -------------
// Examples Part 
// -------------

function openExamples(callback) {
  var JSZip = require('jszip');
  fs.readFile('src/data/synquid-examples.zip', function(err, data) {
    if (err) console.warn(err);
    if (data) {
      JSZip().loadAsync(data).then(callback, 
      function(err) { console.warn(err); });
    }
  });
}

function readExamples(callback) {
  openExamples(function(zip) {
    var examples = {};
    zip.forEach(function(filename) {
      if (mo = /(.*?)-(.*)/.exec(filename)) {
        if (!examples[mo[1]])
          examples[mo[1]] = {name: mo[1], items: {}};
        examples[mo[1]].items[filename] = {name: mo[2]};
      }
    });
    callback(examples);
  });
}

function loadExample(filename, callback) {
  openExamples(function(zip) {
    zip.file(filename).async('string')
    .then(function(data) { callback(data); },
         function(err) { console.warn(err); });
  });
}

$(function() {
  readExamples(function(examples) {
  $.contextMenu({
    selector: '#exec #examples',
    trigger: 'left',
    animation: {duration: 50, show: 'fadeIn', hide: 'fadeOut'},
    callback: function(key, options) {
      loadExample(key, function(programText) {
        window.editor.setValue(programText); 
      });
    },
    events: {show: function(m) { m.trigger = 'none'; },   /* so that second click hides the menu */
             hide: function(m) { m.trigger = 'left'; } },
    zIndex: 91,  /* otherwise clobbered by editor */
    items: examples});
  });
});