/*
 * Buffered handling of a text stream;
 * The stream is read by registering to .data events.
 * A delimiter is provided which is a regex, and whenever it
 * is encountered, all buffered data up until the delimiter
 * becomes a "block" and is sent to the callback.
 */

events = require 'events'


class SplitStream
  (@stream, delimiter, capture=true) ->
    @buf = new SplitBuffer(delimiter, capture)
    @stream.on 'data' ~> @buf.append it

  on: -> @buf.on ...&


class SplitBuffer extends events.EventEmitter

  /**
   * @capture must be equal to the number of capturing groups in @delimiter.
   * Unfortunately that cannot be enforced easily (RegExp does not expose this property, so basically to check it has to be re-parsed).
   */
  (@delimiter, @capture=1) ->
    @buf = ""

  append: (data) ->
    (@buf + data).split @delimiter
      while ..length > 1
        @emit 'block' ..shift!
        for i from 0 til @capture
          if (v = ..shift!)? then @emit 'delimiter' v, i
      @buf = ..0   # last element is what's left after the last delimiter


@ <<< {SplitStream, SplitBuffer}
