require! child_process



class CoqProcess

  launch: ->
    # Hypothesis: sh is the only way to combine stdout and stderr effectively.
    # (without buffering that gets them out of sync)
    coqtop = child_process.spawn 'sh', ['-c', 'coqtop -emacs 2>&1']

    p = new Parser

    @transcript = []

    coqtop.stdout.setEncoding 'utf-8'
    ss = new SplitStream coqtop.stdout, // <prompt>(.*?)</prompt> |
                                           <infomsg>([\s\S]*?)</infomsg> //, 2

    ss.on 'block' @~process-block
    ss.on 'delimiter' (element, i) ~>
      switch i
      | 0 => @process-prompt element
      | 1 => @process-infomsg element

    coqtop.stderr.setEncoding 'utf-8'
    coqtop.stderr.on 'data' (data) ~>
      console.warn "%" + data

    @dispatch = {}  # map of state-id: callback

    window.addEventListener 'unload' ->
      coqtop.stdin.end!

    @coqtop = coqtop

  run-command: (cmd) ->
    p = @current-state
      if !..? then throw new Error "not ready"
    new Promise (resolve, reject) ~>
      @dispatch[p.id + 1] = ~>
        resolve @transcript[@transcript.lastIndexOf(p)+1 to]
      @send-commands cmd

  current-state:~
    -> find-last @transcript, (instanceof StateMark)

  send-commands: (text) ->
    @coqtop.stdin.write text + "\n"

  process-block: (element) ->
    if /^\s*$/.exec element then return

    console.log "Block\n" + element
    if (goals = @parse-goal-descriptors element)?
      console.log "goals:", goals
      block = new ProofModeBlock(element, goals)
    else
      block = new TextBlock(element)

    @transcript.push block

  process-prompt: (element) ->
    prompt = @parse-prompt element
    console.log "prompt:" prompt
    if prompt instanceof StateMark
      @transcript.push prompt
      if (cb = @dispatch[prompt.id])?
        delete @dispatch[prompt.id]
        cb prompt

  process-infomsg: (element) ->
    console.log "info:" element
    @transcript.push new InfoMsg(element)

  #==============
  # Parsing Part
  #==============

  parse-prompt: (prompt-text) ->
    mo = /^(.*?) < (.*?) \|(.*?)\| (.*?) < $/.exec prompt-text
    if mo?
      new StateMark(/*id=*/+mo.2, /*scope=*/mo.3)
    else
      prompt-text

  parse-goal-descriptors: (data) ->
    ngoals = void
    goals = []
    unfocused = void
    for line in data.split '\n'
      # Number of goals
      if (mo = /^(\d+) (focused )?subgoals?(,|$)/.exec line)?
        ngoals = +mo.1
      else if (mo = /^(\d+) (focused )?subgoals? .*?\(ID (\d+)\)/.exec line)?
        ngoals = +mo.1
        goals[0] = +mo.3
      else if (/^No more subgoals/.exec line)?
        ngoals = 0
      # Goal identifiers
      if (mo = /(?:^|, )subgoals? (\d+) \(ID (\d+)\)( is)?/.exec line)?
        goals[mo.1 - 1] = +mo.2
      #else if (mo = /^\(unfocused: ([\d\-]*)\), subgoal (\d+) \(ID (\d+)\)/.exec line)?
      #  unfocused = mo.1.split '-'
      #  goals[mo.2 - 1] = +mo.3

    if ngoals?
      if goals.length != ngoals
        console.warn "Coq reported #ngoals goals, but #{goals.length} goal IDs"

      {goals, unfocused}


class TextBlock
  (@text) ->

class ProofModeBlock extends TextBlock
  (text, @goals) -> super(text)

class InfoMsg
  (@text) ->

class ErrorMsg
  (@text) ->

class StateMark
  (@id, @scope) ->


# The missing functor from Array; finds the last element satisfying p
find-last = (arr, p) ->
  for i from arr.length - 1 to 0 by -1
    if p(v = arr[i]) then return v
find-last-index = (arr, p) ->
  for i from arr.length - 1 to 0 by -1
    if p(arr[i]) then return i


export CoqProcess, TextBlock, ProofModeBlock, InfoMsg, ErrorMsg, StateMark
