


class Markup

  (@cm) ->
    @special-tokens = {}
    
  apply-to-line: (line) ->
    @clear-from-line line
    for tok in @cm.getLineTokens line
      if (lit = @special-tokens[tok.string])
        from = {line, ch: tok.start}
        to = {line, ch: tok.end}
        @cm.markText from, to, {replacedWith: @mk-symbol(lit), handleMouseEvents: true}

  clear-from-line: (line) ->
    from = {line, ch: 0}
    to = {line, ch: @cm.getLine line .length}
    for mark in @cm.findMarks from, to
      mark.clear!

  mk-symbol: (lit) ->
    document.createTextNode lit


$ ->
  cm = new CodeMirror $('#editor').0, do
    line-numbers: true
    content: "text/coq"

  company = new Markup(cm)
    ..special-tokens = {
      '->': '→', '<-': '←', '=>': '⇒', 'fun': 'λ', 
      'forall': '∀', 'exists': '∃', '|-': '⊢',
      '/\\': '∧', '\\/': '∨'}
    
  cm.on \change ->
    #console.log (cm.getLineTokens 20)# .map -> "#{it.string} #{it.type}")
    for line from 0 til cm.lineCount!
      company.apply-to-line line
    
  window <<< {cm}
