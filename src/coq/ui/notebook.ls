

class Notebook
  (@$) ->
    @children = []

  append: (cell) ->
    @children.push cell
    @$.append cell.$
    @



class Cell


class HeadingCell
  ->
    @$ = $ '<h1>'

  text: (txt) -> if txt? then @$.text(txt); @ else @$.text!


class InputCell
  ->
    @$ = $ '<p>' .add-class "prompt"

  val: (txt) -> if txt? then @$.text(txt); @ else @$.text!


class PlainTextCell
  ->
    @$ = $ '<pre>'

  text: (txt) -> if txt? then @$.text(txt); @ else @$.text!



export Notebook, Cell, HeadingCell, InputCell, PlainTextCell
