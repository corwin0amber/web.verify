
class ProofTree
  (@goals, @sequents={}) ->
    @viz-stylesheet = '''
    node [shape=square style=filled fillcolor="#ffffff01"];
    '''
    
  show: ->
    Viz = require "viz.js"  # require here since viz.js takes some time to load
    svg-src = Viz @goals.tree.toDigraph!viz @viz-stylesheet
    # - open a separate window for graph visualization
    w = window.open "./ui/proof-tree.html", "proof-tree"
    @w = w
    setTimeout ~>
    #$(w.document).ready ->
      svg = $(w.document) .find '#tree-container' .html svg-src .find 'svg'
      svg.find('title').remove!  # the tooltips are annoying here
      svg.height "calc(0.5 * #{svg.attr('height')})"  # unfortunately, CSS
      svg.width  "calc(0.5 * #{svg.attr('width')})"   # cannot do that yet
      svg.click "g" (ev) ~>
        g = $(ev.target).closest("g")
        if (mo = /^u(\d+)$/.exec g.attr('id'))?
          sequent = @sequents[mo.1]
          #@goals.find-sequent parseInt(mo.1)
          $(w.document) .find '#sequent-container'
            ..empty!
            if sequent?
              ..append sequent.$.clone!
    , 100



export ProofTree