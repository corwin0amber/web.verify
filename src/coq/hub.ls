path = require 'path'

input-fn = path.join projdir, 'data/exercise.v'
#input-fn = path.join process.env.HOME, 'var/workspace/various-proofs/funcprogs/sum-alt.v'

/*
 * TODO currently assuming that ready() handlers for coq.ls and ui/editor.ls
 * have been called before this one.
 */
$ ->
  input-text = fs.readFileSync input-fn, 'utf-8'
  proj-path ?= path.dirname input-fn

  coq = new Coq
  export coq

  coq.launch input-text, proj-path
  cm.setValue input-text

  cm.on \change ->
    coq.reparse cm.getValue!



# -------------------------------------------------------------------
#  Scaffolding phase
->
  /* Initialize Coq */
  coq = new CoqProcess
  coq.launch!

  export coq

  /* Initialize Notebook */
  nb = new Notebook($('#top'))

  export nb

  fwd = (cmd) ->
    nb.append new InputCell!val(cmd)
    coq.runCommand cmd
    .then ->
      for el in it
        switch
        | el instanceof ProofModeBlock => nb.append new PlainTextCell!text el.text

  export fwd
