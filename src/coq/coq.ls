
fs = require 'fs'
path = require 'path'
{spawn} = require 'child_process'
{flatten, fold, zip} = require 'prelude-ls'



class Coq

  ->
    @options = {printing: {implicit: false}}
    @current-state = {scope: ""}
    @latest-state = {id: 0}  # the state with the highest id ever seen
    @top = NQ $ '#top'
    @current-container = @top
    @script-container = $ '#script'
    @queue = []
    @script = []
    @expect = void
    @pp = new PrettyPrint @options

    @setup-events!

  prologue: (proj-path) ->
    proj-fn = path.join proj-path, '_CoqProject'
    try
      pro = fs.readFileSync proj-fn, 'utf-8'
    catch
      pro = ""
    """#{pro}\nAdd LoadPath "#{proj-path}".\n"""

  metadata: (proj-path) ->
    meta-fn = path.join proj-path, 'metadata.json'
    try
      meta = fs.readFileSync meta-fn, 'utf-8'
    catch
      return {}
    JSON.parse meta

  launch-file: (input-fn, proj-path) ->
    input-text = fs.readFileSync input-fn, 'utf-8'
    proj-path ?= path.dirname input-fn
    @launch input-text, proj-path

  launch: (input-text, proj-path) ->
    if proj-path?
      prologue = @prologue proj-path
      @pp.load-metadata @metadata proj-path
    else
      prologue = ""
    @_launch input-text, prologue

  _launch: (input-text, prologue="") ->
    coq = spawn 'sh', ['-c', 'coqtop -emacs 2>&1']

    p = new Parser

    @goals = T(0)

    coq.stdout.setEncoding 'utf-8'
    ss = new SplitStream coq.stdout, // <prompt>(.*)</prompt> //, 1

    ss.on 'block' @~process-block
    ss.on 'delimiter' @~process-prompt

    coq.stderr.setEncoding 'utf-8'
    coq.stderr.on 'data' (data) ~>
      console.warn "%" + data

    window.addEventListener 'unload' ->
      coq.stdin.end!

    @process = coq

	# Split input text into (max.) three sections:
	# - setup is run as initialization (output is discarded)
	# - queue is run one command at a time (commands are echoed along with output)
	# - script is stacked below and can be stepped through
    [setup, queue, ...script] = input-text.split /\(\*-------+\*\)/

    if !queue? then [setup, queue, script] = ['', '', [setup]]
    else if !script.0? then [setup, queue, script] = [setup, '', [queue]]

    prep = ~> @split-commands @strip-comments it

    @queue = prep (queue ? '') .map -> [it, {}]
    @script = flatten (script ? '').map prep
    @options-setup! + prologue + setup
      console.log "-- Setup --\n#{..trim!}\n--------"
      @send-commands .. + "Definition setup_done := True."

  reparse: (input-text) ->
    commands = @split-commands @strip-comments input-text
    prompts = (@top.dfs '.prompt' .$.toArray!) ++ \
              (@script-container.children '.prompt' .toArray!)
    # filter out empty prompts
    prompts = prompts.filter -> $(it).text!trim! != ""
    # zip and match
    for [cmd, pmt] in zip commands, prompts
      if cmd != $(pmt).text!
        $(pmt).text cmd

  options-setup: ->
    flag = -> if it then "Set" else "Unset"
    """
    Unset Printing Notations.
    Unset Printing Records.
    Unset Printing Projections.
    Set Printing Coercions.
    Set Printing Synth.
    #{flag @options.printing.implicit} Printing Implicit.

    """

  process-block: (element) ->
    console.log element

    goal-desc = @parse-goal-descriptors element
    sequents = p.find-sequents element

    clone-goals = (goals) ->
      tree: goals.tree.clone!
      active: goals.active

    goals = clone-goals @last-goals!

    if goal-desc?
      active = goals.tree.find-path goals.active

      # Add newly introduced goals from goal-desc
      all-goals = goals.tree.nodes.map (.root)
      for goal-id in goal-desc.goals
        if goal-id not in all-goals
          active[*-1].subtrees.push T goal-id
      # Remove already-discharged leaves from goals
      #for node in goals.nodes.reverse!
      #  node.subtrees .= filter -> !it.is-leaf! || it.root in goal-desc.goals

      goals.active = goal-desc.goals[0] ? 0

      # Adjust containers to new goal branch
      prev-active = active
      active = goals.tree.find-path goals.active

      if prev-active[*-1].subtrees.length > 1
        @current-container.$.add-class 'splitting'

      @clean-up!

      console.log prev-active.map((.root)), active.map((.root)), goals.active

      /*
      for n in prev-active
        if n not in active
          @current-container = @current-container.parent!
      for n in active
        if n not in prev-active
          next-container = NQ <| $ '<div>' .add-class 'container'
            @current-container.append ..
          @current-container = next-container
      */
    @goals = goals

    if sequents.length > 0
      @clean-up!
      @current-container.append do
        for sequent in sequents
          NotebookQuery.mk @pp.display-sequent sequent
            ..root.goals = clone-goals goals
            ..$.prepend @make-goal-tree-tag ..

    errors = @find-errors element
    if errors.length > 0
      @clear-errors!
      for err in errors
        @current-container.append <| do
          j = $ '<div>' .add-class 'error' .text err
        j.0.scrollIntoViewIfNeeded!  # Webkit-specific method

    if element == /setup_done/
      @expect = {} <<< @latest-state
        ..id += 1  # @@ a bit hackey

  process-prompt: (element) ->
    element = @parse-prompt element

    console.log element, @expect

    if element.scope !== @current-state.scope
      if element.scope.length > @current-state.scope.length
        last = @current-container.last!remove!
        parent = @current-container #.parent!
        wrap-container = NQ <| $ '<div>' .add-class 'container proof' .append ($ '<h1>' .text element.scope)
          parent.append ..
          ..append last
          #..append (@current-container = @current-container.remove!)
        @current-container = wrap-container
      else
        @current-container = @current-container.closest '.proof' .parent!

    @current-state = {} <<< element
    if element.id > @latest-state.id
      @latest-state = {} <<< element

    last = {prompt: @last-prompt!}
      ..state = ..prompt.$.data 'state'
    if last.state !== @current-state \
        || last.prompt.is-empty! # $('.prompt').length == 0
      @clean-up!

      #@clear-errors!
      #if last.prompt.is-empty! || last.state
      @make-prompt! .append-to @current-container
        @scroll-into-view ..$
        ..$.focus!

    if @expect? && element.id >= @expect.id  # @@@ >= is very bad! but what can we do? sometimes id is inc'd internally
      @replay-next!

  #
  #-- This was the main part. Now smaller fragments of functionality ensue --
  #

  strip-comments: (text) ->
    text.replace /\(\*[\S\s]*?\*\)/g ''

  split-commands: (text) ->
    text.replace /(\.(?=[ \t]*\n))/g '$1^^^'
    .split /\^\^\^[ \t]*\n/ .filter (!= /^\s*$/)

  find-errors: (data) -> []
    if (mo = // characters \s (\d+-\d+): \n (>.*\n)+ ([\s\S]*?)\n\s*\n //.exec data)
      ..push mo.3

  parse-prompt: (prompt-text) ->
    mo = /^(.*?) < (.*?) \|(.*?)\| (.*?) < $/.exec prompt-text
    if mo?
      {scope: mo.3, id: +mo.2}
    else
      prompt-text

  parse-goal-descriptors: (data) ->
    ngoals = void
    goals = []
    unfocused = void
    for line in data.split '\n'
      # Number of goals
      if (mo = /^(\d+) (focused )?subgoals?(,|$)/.exec line)?
        ngoals = +mo.1
      else if (mo = /^(\d+) (focused )?subgoals? .*?\(ID (\d+)\)/.exec line)?
        ngoals = +mo.1
        goals[0] = +mo.3
      else if (/^No more subgoals/.exec line)?
        ngoals = 0
      # Goal identifiers
      if (mo = /(?:^|, )subgoal (\d+) \(ID (\d+)\)( is)?/.exec line)?
        goals[mo.1 - 1] = +mo.2
      #else if (mo = /^\(unfocused: ([\d\-]*)\), subgoal (\d+) \(ID (\d+)\)/.exec line)?
      #  unfocused = mo.1.split '-'
      #  goals[mo.2 - 1] = +mo.3

    if ngoals?
      if goals.length != ngoals
        console.warn "Coq reported #ngoals goals, but #{goals.length} goal IDs"

      {goals, unfocused}

  send-commands: (text) ->
    @process.stdin.write text + "\n"

  clean-up: ->
    $$ = (sel) ~> @top.find sel
    $$ '.error' .remove!
    $$ '.outdated' .remove!
    $$ '.prompt' .$ .attr 'readonly' true
    @current-container.find '.prompt' .filter (-> it.$.text! == "") .remove!

  clear-errors: ->
    $$ = (sel) ~> @top.find sel
    $$ '.error' .remove!

  last-prompt: ->
    @current-container.last('.prompt')

  last-goals: ->
    if !(prev = @current-container.last (.root.goals?)).is-empty!
      prev.root.goals
    else
      tree: T(0)
      active: 0

  setup-events: ->
    @script-container.on \dblclick '.prompt' (event) ~>
      @enqueue-until $(event.target)

  make-prompt: ->
    nq = NQ <| do
      $ '<p>' .add-class "prompt"
        ..keydown (event) ~>
          if !..attr 'readonly'
            if event.which == 13
              if ..text! != /[.+*\-{}]\s*$/ then ..text ..text! + "."
              ..data 'state' @current-state
              @send-commands ..text!
              event.preventDefault!
            else if event.which == 8 && ..text! == ""   # BACKSPACE
              @undo-last nq.previous-deep '.prompt'
              event.preventDefault!
            else if event.which == 46 && ..text! == ""     # DELETE
              @next-from-script!remove!
            else if event.which == 38                          # UP
              if ..text! == ""
                x = nq.previous-deep '.prompt'
                if ! x.is-empty!
                  @record-last x ; @undo-last x
              else
                @record-last nq ; ..text "" ; @clear-errors!
              event.preventDefault!
            else if event.which == 40 && ..text! == ""       # DOWN
              @enqueue-next!
              event.preventDefault!
        ..dblclick (event) ~>
          @record-last nq ; @undo-last nq

  make-goal-tree-tag: (sequent) ->
    goals = sequent.root.goals
    $ '<span>' .add-class 'tag' .text goals.tree.toString!
      ..click ~>
        proof = sequent.closest '.proof'
        sequents = @collect-goals proof
        new ProofTree(goals, sequents).show!
        #find-sequent = ~> @find-goal proof, it
        #new ProofTree(^^goals <<< {find-sequent}).show!

  find-goal: (proof-container, goal-id) ->
    proof-container.last-deep (.root.goals?active == goal-id)

  collect-goals: (proof-container) ->
    {}
      proof-container.find (.root.goals?active) .for-each ->
        ..[it.root.goals.active] = it

  scroll-into-view: (jdom, clearance=50) ->
    element-bottom = jdom.offset().top + jdom.height() - $('body').scrollTop() + $('body').offset().top
    window-bottom = $('body')[0].clientHeight
    if (delta = window-bottom - element-bottom - clearance) < 0
      window.scrollBy 0, -delta

  count-commands: (text) ->
    r = /[^.]\.(?!\S)/g ; c = 0
    while r.exec text then c += 1
    c

  skip-focusing: (command) ->
    command.replace /^\s*[+*\-{}]/ ''

  replay-next: ->
    cmd = @queue.shift!
    if cmd?
      [cmd, meta] = cmd
      meta.started?!
      @expect = {} <<< @latest-state
        ..id += @count-commands cmd
        ..meta = meta
      @last-prompt!$
        ..text cmd
        ..data 'state' @current-state
      @send-commands @skip-focusing cmd
    else
      @expect = void
      @script-container.append @script.map ->
        $ '<p>' .add-class 'prompt' .text it
      @script = []

  record-last: (starting) ->
    prompts = starting.onward '.prompt'
    @script-container.prepend prompts.$.map ->
      if (text = $(@).text!) != /^\s*$/
        $ '<p>' .add-class 'prompt' .text text .get!

  undo-last: (starting) ->
    state = starting.$.data 'state'
    if state?
      @clean-up!
      starting.$.prev 'div:not(.container)' .add-class "outdated"
      starting.truncate-deep starting.closest '.proof'
      @current-state = state
      @current-container = starting.closest '.container'
      @send-commands "BackTo #{state.id}."

  enqueue: (cmd, meta={}) ->
    @queue.push [cmd, meta]
    if !@expect? then @replay-next!

  enqueue-next: ->
    cmd = @next-from-script!remove!
    if cmd.length then @enqueue cmd.text!

  /**
   * Runs all the commands in the script up to (not including) the
   * specified one.
   * @param prompt - should be a ".prompt" element that is a child of @script-container
   */
  enqueue-until: (prompt) ->
    console.assert (prompt.0 in @script-container.children!), "enqueue-until: target prompt no longer in script", prompt
    backlog = Array.prototype.reverse.call(prompt.prevAll!)
      #..remove!
      for let pmt in ..
        @enqueue $(pmt).text!, started: -> $(pmt).remove!

  next-from-script: ->
    @script-container.children('.prompt').first!

  configure-context-menu: ->
    $.contextMenu do
      selector: '.sequent .premise'
      callback: (key, opt) ~> @enqueue "#key #{$(opt.$trigger).find('.name').text!}."
      items:
        rewrite: {name: "rewrite", icon: "edit"}
    $.contextMenu do
      selector: '.sequent .goal'
      callback: (key, opt) ~> @enqueue "#key."
      items:
        simpl: {name: "simpl", icon: "cut"}
        trivial: {name: "trivial", icon: "quit"}

  export-notebook: ->
    h = $ 'html' .clone!
      ..find 'script' .remove!
      ..find 'head' .append do
        $ '<link>' .attr rel: 'stylesheet', type: 'text/css', href: 'fonts.css'
      ..find 'input' .each -> $(@).attr 'value' $(@).val!
    fs.writeFileSync '/tmp/a.html', "<html>\n#{h.html!}\n</html>"



class NotebookQuery

  (@paths) ->

  $:~
    -> $ @paths.map -> it[*-1].root.jdom[0]

  is-empty: -> @paths.length == 0

  is-in: (other) -> other? && @paths.every (x) -> other.paths.some (y) -> x[*-1] == y[*-1]
  not-in: (other) -> ! @is-in(other)

  for-each: (op) ->
    for path in @paths
      op new NotebookQuery( [path] )

  not: (other) ->
    if !other? then @
    else new NotebookQuery @paths.filter (x) -> !other.paths.some (y) -> x[*-1] == y[*-1]

  assert-single: (method) ->
    if @paths.length > 1
      forwhat = if method? then " for '#method'" else ""
      throw Error "too many nodes#forwhat (required <= 1, got #{paths.length})"

  root:~
    -> @assert-single 'root' ; @paths[0][*-1].root

  @mk = (thing) ->
    if thing instanceof $
      new NotebookQuery [[T jdom: thing]]
    else if thing instanceof Node
      new NotebookQuery [[T jdom: $ thing]]
    else if thing instanceof Tree
      new NotebookQuery [[thing]]
    else if thing instanceof NotebookQuery
      thing
    else
      throw Error thing

  append: (thing) ->
    @assert-single 'append'
    forest = []
    mk = (thing) ->  # :( some duplication here
      if thing instanceof Array || thing instanceof $
        for x in thing then mk x
      else if thing instanceof Node
        forest.push new NotebookQuery [[T jdom: $ thing]]
      else if thing instanceof Tree
        forest.push new NotebookQuery [[thing]]
      else if thing instanceof NotebookQuery
        forest.push thing
      else
        throw Error thing
    mk thing

    for p in @paths
      for q in forest
        p[*-1].root.jdom.append <| q.paths.map (.[*-1].root.jdom.0)
        p[*-1].subtrees.push ...(q.paths.map (.[*-1]))
        q.paths = q.paths.map (p ++)

  append-to: (nq) ->
    nq.append @
    @

  or-else: (otherwise) ->
    if @isEmpty! then otherwise else @

  parent: ->
    new NotebookQuery (@paths.map (.[til -1]) .filter (.length))

  children: ->
    flat-map = (f, arr) -> []
      for el in arr then ..push ...f(el)
    new NotebookQuery flat-map (path) ->
      path[*-1].subtrees.map -> path ++ [it]
    , @paths

  last: (selector=->true) ->
    selector = _sel selector

    found = void
    for child in @children!paths
      if selector (nq = new NotebookQuery [child])
        found = nq

    found ? new NotebookQuery []

  since-last: (selector=->true) ->
    last = @last selector
    if last.is-empty! then @children! else last.next-all!

  /**
   * Finds the last descendant (in a postorder walk)
   * that matches the given selector.
   */
  last-deep: (selector=->true) ->
    selector = _sel selector

    found = void
    for child in @children!paths
      if selector (nq = new NotebookQuery [child])
        found = nq
      else
        found = nq.last-deep selector .or-else found

    found ? new NotebookQuery []

  previous: (selector=->true) ->
    if @is-empty! then return @
    @assert-single 'previous'

    selector = _sel selector

    me = @paths[0][*-1]
    found = void
    for sibling in @parent!children!paths
      if sibling[*-1] == me then break
      if selector (nq = new NotebookQuery [sibling])
        found = nq

    found ? new NotebookQuery []

  /**
   * Finds the previous element in a (postorder) walk of the
   * ancestor that matches the given selector.
   */
  previous-deep: (selector=->true, ancestor) ->
    if @is-empty! then return @
    @assert-single 'previous-deep'

    selector = _sel selector

    me = @paths[0][*-1]
    found = void
    for sibling in @parent!children!paths
      if sibling[*-1] == me then break
      if selector (nq = new NotebookQuery [sibling])
        found = nq
      else
        found = nq.last-deep selector .or-else found

    found ? @parent!not(ancestor).previous-deep selector

  next: (selector=->true) ->
    if @is-empty! then return @
    @assert-single 'next'

    selector = _sel selector

    me = @paths[0][*-1]
    found = void
    commence = false
    for sibling in @parent!children!paths
      if sibling[*-1] == me then commence=true
      else if commence && selector (nq = new NotebookQuery [sibling])
        found = nq
        break

    found ? new NotebookQuery []

  closest: (selector=->true) ->
    selector = _sel selector

    x = []
      for path in @paths
        w = path[to]
        while w.length > 0
          if selector new NotebookQuery [w] then ..push w ; break
          w.pop!

    new NotebookQuery x

  _sel = (sel) ->
    if $.isFunction(sel) then sel else -> it.$.is sel

  _subpaths = (path) -> path[*-1].subtrees.map -> path ++ [it]

  _dfs = (paths, pred) -> []
    walk = (path) ~>
      if pred new NotebookQuery [path]
        ..push path
      for p in _subpaths(path) then walk p
    for p in paths then walk p

  filter: (selector) ->
    new NotebookQuery @paths.filter -> selector new NotebookQuery [it]

  dfs: (selector=->true) ->
    new NotebookQuery _dfs @paths, _sel selector

  find: (selector) ->
    selector = _sel selector
    @dfs ~> it.not-in(@)  and  selector it

  next-all: (selector=->true) ->
    if @is-empty! then return
    @assert-single 'next-all'

    selector = _sel selector

    me = @paths[0][*-1]
    commence = false
    x = []
      for sibling in @parent!children!paths
        if commence then ..push sibling
        if sibling[*-1] == me then commence = true

    new NotebookQuery x

  onward: (selector=->true) ->
    if @is-empty! then return
    @assert-single 'onward'

    selector = _sel selector

    me = @paths[0][*-1]
    commence = false
    x = []
      for sibling in @parent!children!paths
        if sibling[*-1] == me then commence = true
        if commence then ..push sibling

    new NotebookQuery x .dfs selector


  /**
   * Removes everything from this point to the end of the parent.
   */
  truncate: ->
    if @is-empty! then return
    @assert-single 'truncate'

    me = @paths[0][*-1]
    to-remove = []
      for sibling in @parent!children!paths
        if sibling[*-1] == me || ..length then ..push sibling

    new NotebookQuery(to-remove).remove!

  truncate-next: ->
    @next-all!remove!

  truncate-deep: (ancestor) ->
    if @is-empty! then return
    @truncate!
    @parent!not(ancestor).truncate-next-deep ancestor

  truncate-next-deep: (ancestor) ->
    if @is-empty! then return
    @truncate-next!
    @parent!not(ancestor).truncate-next-deep ancestor

  remove: ->
    for path in @paths
      node = path[*-1]
      $ node.root.jdom .remove!
      if (parent = path[*-2])?
        parent.subtrees = parent.subtrees.filter (!= node)

    new NotebookQuery @paths.map -> [it[*-1]]


NQ = (thing) ->
  if (thing) instanceof $     # must be all new/detached nodes
    new NotebookQuery thing[to].map -> [ T jdom: $(it) ]
  else if (thing) instanceof Tree
    new NotebookQuery [[thing]]
  else
    new NotebookQuery thing



export Coq
