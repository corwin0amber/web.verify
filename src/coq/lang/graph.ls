
/**
 * (directed; undirected tbd)
 */
class Graph
  (@nodes=[], @edges=[]) ->
    
  viz: (stylesheet="") ->
    node-name = -> "u#it"
    node-id = -> "u#it"
    node-label = -> "#it"
    fmt-node = -> "#{node-name it} [id=#{node-id it}, label=#{node-label it}];"
    fmt-edge = ([u,v]) -> "#{node-name u} -> #{node-name v};"
    """
    digraph {
      #{stylesheet}
      #{@nodes .map fmt-node .join "\n  "}
      #{@edges .map fmt-edge .join "\n  "}
    }
    """


export Graph
