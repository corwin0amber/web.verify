
{flatten} = require 'prelude-ls'
nearley = require 'nearley'


#trc = (lbl, d, ...args) -> console.log("#lbl:  [#{d.map (?.toString!)}]   (#{args})")
trc = ->  
  
class Parser
  KEYWORDS = <[ match return end ]>

  just: (idx) ~> (d) ~> trc("just", d, idx) ; d[idx]
  join-all: (d) ~> if d?.map then d.map @join-all .join "" else d || ''
  
  identifier: (d, _, reject) ~> ja = @join-all(d) ; if ja in KEYWORDS then reject else T(ja)
  app: (fidx, argidx) ~> (d) ~> trc("app", d, fidx, argidx) ; T("@", flatten [d[fidx], d[argidx]])
  quant: (quantidx, vaidx, bodyidx) ~> (d) ~> trc("quant", d) ; T(d[quantidx], flatten [d[vaidx], d[bodyidx]])
  op: (opidx, argidxs) ~> (d) ~> trc("op", d) ; T(d[opidx], flatten argidxs.map (d.))
  concat: (...idx) ~> (d) ~> 
    trc("concat", d, idx)
    ls = idx.map -> if (l = d[it]).length? then l else [l]
    [].concat ...ls
  match: (hdridx, casesidx) ~> (d) ~> trc("match", d) ; T(d[hdridx].root, d[hdridx].subtrees ++ d[casesidx])
  matchhdr: (opidx, argidxs) ~> (d) ~> trc("matchhdr", d) ; T(d[opidx], flatten argidxs.map (d.) .filter -> it?)
  fixproto: (argsidx, retidx) ~> (d) ~> trc("fixproto", d) ; [T("return").of(d[retidx]), ...d[argsidx]]

  find-sequents: (text) -> []
    spc = '[ \t]'
    re = // \n\s*\n ((\s.+\n)*?)#spc*=====+\n((?:#spc+\S.*\n)+) \s*(?:\n|$) |
            (?:\n|^) (?:\S+#spc*|#spc+)=\n?((?:#spc*\S.*\n)+) \s*(?:\n|$) |
            (?:\n|^) (\S.*\n\s+:(?:#spc+\S.*\n)+) \s*(?:\n|$) |
            (?:\n|^) (\S+#spc:\s(?:\S.*\n)+) \n |
            (?:\n|^) (No\smore\ssubgoals.) \n //g
    while (mo = re.exec text)
      ..push do
        if mo.1?
          env: (@split-blocks @dedent mo.1).map @~protected-parse
          goal: @protected-parse mo.3.trim!
        else if (t = mo.4 ? mo.5 ? mo.6)?
          env: [@protected-parse t.trim!]
        else
          env: []
      
  dedent: (text) ->
    lines = text.split '\n'
    while lines.every (== /^(\s|$)/) and lines.some (.length)
      lines = lines.map (.substring 1)
    lines.join '\n'
    
  split-blocks: (text) ->
    text.split /\n+(?!\s)/ .filter (!= /^\s*$/)

  protect: (default-value, op) ->
    try op!
    catch err
      console.error err.message
      default-value
    
  UNABLE = T("unable-to-parse")
  
  protected-parse: (term-text) ->
    @protect UNABLE, ~> @parse term-text
  
  parse: (term-text) ->
    p = new nearley.Parser grammar.ParserRules, grammar.ParserStart
    try
      p.feed term-text
      if p.results.length == 0 then throw new Error("syntax error (in '#term-text')")
      if p.results.length > 1 #&& (p.results.some -> !it.equals(p.results[0]))
        throw new Error("ambiguous grammar (in '#term-text')")
    catch e
      console.log term-text
      #debug-grammar p
      throw e
    p.results[0]

  debug-grammar: (p) ->
    for col,i in p.table
      console.log "At #i"
      for state in col
        console.log "    " + state.toString!
    if p.results?
      console.error(p.results.map (.toString!))
    
  

@p = new Parser

@ <<< {Parser}
