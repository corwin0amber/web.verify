@builtin "whitespace.ne"

d -> term                                           {% id %}
    | terms _ ":" _ term                  {% p.op(2,[0,4]) %}

term -> term4                                       {% id %}

term4 -> term3                                      {% id %}
    | "forall" __ binders _ "," _ term4 {% p.quant(0,2,6) %}
    | "fun" __ binders _ "=>" _ term4   {% p.quant(0,2,6) %}

term3 -> term2                                      {% id %}
    | term3 _ "->" _ term2               {% p.op(2,[0,4]) %}

term2 -> term1   {% id %}

term1 -> term0                                      {% id %}
    | term1 __ term0                        {% p.app(0,2) %}
    
term0 -> var                                        {% id %}
    | "(" _ term _ ")"                       {% p.just(2) %}
    | matchexpr                                     {% id %}
    | fixexpr                                       {% id %}

terms -> term                                       {% id %}
    | term _ "," _ terms                 {% p.concat(0,4) %}

binders -> binder                                   {% id %}
    | binderpws:* binderp                {% p.concat(0,1) %}
    
binderp -> "(" _ binder _ ")"                {% p.just(2) %}
binderpws -> binderp _                       {% p.just(0) %}

binder -> vars _ ":" _ term              {% p.op(2,[0,4]) %}

var -> "@":? identifier                      {% p.just(1) %}

vars -> var                                         {% id %}
    | var __ vars                        {% p.concat(0,2) %}
    | var _ "," _ vars                   {% p.concat(0,4) %}

matchexpr -> matchhdr matchcase:* 
             _ "end"                      {% p.match(0,1) %}
matchhdr -> "match" __ term
            matchret:? __ "with"   {% p.matchhdr(0,[2,3]) %}
matchret -> __ "return" __ term            {% p.op(1,[3]) %}
matchcase -> _ "|" _ term _ "=>" _ term  {% p.op(5,[3,7]) %}

fixexpr -> "fix" __ var __ fixdef        {% p.op(0,[2,4]) %}
fixdef ->   fixproto _ ":=" _ term       {% p.op(2,[0,4]) %}
fixproto -> binders:? _ ":" _ term     {% p.fixproto(0,4) %}

####################################
####### TOKENS FOR TOKENIZER #######
####################################

identifier -> "?":? letter idrest         {% p.identifier %}

idrest -> idch:*

idch -> letter
	| digit
    | [_.']

## unicode ranges for letter regex taken from http://stackoverflow.com/questions/150033/regular-expression-to-match-non-english-characters
letter -> [a-zA-Z$_\u00C0-\u00D6\u00D8-\u1FFF\u2080-\u2089\u2C00-\uD7FF] {% id %}
digit -> [0-9] {% id %}

num -> digit:+ {% function(d) { return parseInt(d[0].join("")); } %}

op -> validStandaloneOpchars
	| opchar opchar:+

validStandaloneOpchars -> [!%&*+<>?^|~\\\-] {% id %}
opchar -> validStandaloneOpchars {% id %}
	| [=#@\:] {% id %}

# _ represents optional whitespace; __ represents compulsory whitespace
#_ -> [\s]:*    {% function(d) {return null; } %}
#__ -> [\s]:+   {% function(d) {return null; } %}

___ -> [\s]:*    {% function(d) {return null; } %}

arrow -> "↦"
leftparen -> "("
rightparen -> ")"
leftbracket -> "⟨"
rightbracket -> "⟩"
leftsquarebracket -> "["
rightsquarebracket -> "]"
underscore -> "_"
forwardslash -> "/"
colondash -> ":-"
backtick -> "`"
subseteq -> "⊆"
colon -> ":"
comma -> ","
typeArrow -> "->" {% id %} | "→" {% function() { return "->"; } %}
fix -> "fix"