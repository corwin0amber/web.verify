fs = require 'fs'


RECOGNIZED_GLOBAL_KINDS = <[ ind constr prf rec proj ]>

read-globals = (glob-text) -> []
  for ln in glob-text.split '\n'
    [kind, loc, ns, name] = ln.split /\s/
    if kind in RECOGNIZED_GLOBAL_KINDS && ns == "<>"
      ..push [kind, name]

read-globals-files = (glob-filenames) -> 
  [].concat ...do
    for fn in glob-filenames
      read-globals fs.readFileSync fn, 'utf-8'


@ <<< {read-globals, read-globals-files}
