Set Implicit Arguments.
Require Import List.
Import ListNotations.
(* Require Import Arith. *)
(* Require Import Coq.Numbers.Natural.Peano.NPeano. *)

(*----------------------------------------------------------------*)

(*----------------------------------------------------------------*)
Lemma app_nil_l: forall T (l : list T), [] ++ l  = l.
Proof.
intros T l.
reflexivity.
Qed.

Lemma app_nil_r: forall T (l : list T), l ++ []  = l.
induction l.
+ trivial.
+ simpl.
  rewrite IHl.
  trivial.
Qed.
