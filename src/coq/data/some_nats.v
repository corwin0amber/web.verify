Set Printing All.
Require Import Arith.
Require Import Coq.Numbers.Natural.Peano.NPeano.

Section Name.

  Variable A: Set.

  Fixpoint k n :=
    match n with
    | O => 0
    | S n' => 1
    end.

  Variable ψ : A.
(*----------------------------------------------------------*)

  Lemma zero : forall n, 0 <= n.
    intros. induction n.
    reflexivity.
    apply le_S.
    assumption.
  Qed.

(*----------------------------------------------------------*)
  Lemma shrink : forall n m, S n <= m -> n <= m.
  Proof.
    intros.
    induction H.
    apply le_S; reflexivity.
    apply le_S; assumption .
  Qed.

  Lemma pike : forall x y, S x <= S y -> x <= y.
    intros.
    inversion H.
    reflexivity.
    apply shrink.
    assumption.
  Qed.

  Lemma poseidon : forall n : nat, 2 * (S n) = S (S (2 * n)).
  Check @eq.
    intros.
    rewrite <- mult_n_Sm.
    rewrite plus_comm.
    simpl.
    reflexivity.
  Qed.
  
  Check poseidon.
  Check @eq.
    
End Name.


