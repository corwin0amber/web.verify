(** * 6.887 Formal Reasoning About Programs, Spring 2016 - Pset 3 *)

Require Import Frap Pset3Sig.

Set Implicit Arguments.

Definition abstracts state (preds : fmap var (state -> Prop)) st pm :=
  forall x b, pm $? x = Some b
              -> match preds $? x with
                 | None => False
                 | Some pr => pr st <-> b = true
                 end.

(*----------------------------------------------------------------*)

Lemma assumptionsAccurate_forall :
  forall state (preds : fmap var (state -> Prop))
         st assumptions,
    (forall a, In a assumptions -> assertionAccurate preds a st) ->
    assumptionsAccurate preds assumptions st.
Proof.
  induct assumptions; simplify.
  + trivial.
  + propositional.
    - apply H.
      propositional.
    - apply IHassumptions.
      simplify; apply H; propositional.
Qed.

Lemma assertionHolds_ok : forall pm a,
assertionHolds pm a = true ->
pm $? AssumedPredicate a = Some (AssumedToBe a).
Proof.
  simplify.
  cases a.

  simplify.  
  cases (pm $? AssumedPredicate).
  + f_equal. cases b; cases AssumedToBe; equality.
  + equality.
Qed.

    
Lemma assumptionsHold_forall : forall (st : fmap var bool) assumptions,
    assumptionsHold st assumptions = true ->
    forall a, In a assumptions -> assertionHolds st a = true.
Proof.
  induct assumptions; simplify; try equality.
  rewrite andb_true_iff in H.
  propositional; equality || eauto.
Qed.

Lemma abstracts_accurate : forall state (preds : fmap var (state -> Prop))
                                 st pm assumptions,
  abstracts preds st pm ->
  assumptionsHold pm assumptions = true ->
  assumptionsAccurate preds assumptions st.
Proof.
  simplify.
  apply assumptionsAccurate_forall.
  unfold assertionAccurate.
  unfold abstracts in H.
  simplify.
  apply H.
  apply assertionHolds_ok.  
  eapply assumptionsHold_forall.
  apply H0.
  assumption.  
Qed.

Lemma transition_accurate_rule : forall state (r : rule) (preds : fmap var (state -> Prop)) doIt,
    ruleAccurate preds doIt r ->
    forall st st' pm pm' pm'', abstracts preds st pm -> doIt st st' ->
                               abstracts preds st' pm' ->
                               pm'' = applyRule pm pm' r ->
                               abstracts preds st' pm''.
Proof.
  simplify.
  rewrite H3.
  intros x b.
  unfold applyRule. cases r.
  cases (assumptionsHold pm Assumptions).
  + (* in case rule assumptions hold *)
    cases Conclusion.
  
    simplify.
    cases (x ==v AssumedPredicate).
    (* destruct (decide (x = AssumedPredicate)). *)
    
    - rewrite <- e in H4.
      rewrite lookup_add_eq in H4; try equality.
      invert H4.
      unfold ruleAccurate in H; simplify.
      unfold assertionAccurate in H; simplify.
      apply (H st); try assumption.
      eapply abstracts_accurate; eauto.

    - rewrite lookup_add_ne in H4; try equality.
      apply H2; equality.

  + (* in case rule assumptions do not hold *)
    simplify.
    apply H2; equality.
Qed.


Lemma transition_accurate' : forall state (rules : list rule) (preds : fmap var (state -> Prop)) doIt,
    (forall r, In r rules -> ruleAccurate preds doIt r) ->
    forall st st' pm pm' pm'', abstracts preds st pm -> doIt st st' ->
                               abstracts preds st' pm' ->
                               pm'' = applyRules pm pm' rules ->
                               abstracts preds st' pm''.
Proof.
  induct rules; simplify.

  + rewrite H3; equality.
  + eapply IHrules with (pm' := applyRule pm pm' a).
    simplify; apply H; propositional.
    apply H0.
    apply H1.
    eapply transition_accurate_rule.
    apply H; propositional.
    apply H0.
    apply H1.
    apply H2.
    equality.
    equality.
Qed.



Lemma transition_accurate : forall state (rules : list rule) (preds : fmap var (state -> Prop)) doIt,
    (forall r, In r rules -> ruleAccurate preds doIt r) ->
    forall st st' pm pm', abstracts preds st pm -> doIt st st' ->
                          pm' = applyRules pm $0 rules ->
                          abstracts preds st' pm'.
Proof.
  simplify.
  eapply transition_accurate'; eauto.
  unfold abstracts; simplify; invert H3.
Qed.


Lemma paR_abstracts : forall pc state action
                             (pc0 : pc) (st0 : state) pc0' st0'
                             (pa : predicate_abstraction state action),
                             paR pa (pc0, st0) (pc0', st0') ->
                             abstracts pa.(Predicates) st0 st0'.
Proof.
  simplify.
  induct H.
  unfold abstracts. apply H.
Qed.


Theorem predicate_abstraction_simulates : forall pc state action
  (pc0 : pc) (st0 : state)
  (actionOf : pc -> action -> pc -> Prop)
  (doAction : action -> state -> state -> Prop)
  (pa : predicate_abstraction state action),
  predicate_abstraction_sound doAction pa
  -> simulates (paR pa)
               (actionSys pc0 st0 actionOf doAction)  (* [sys1] *)
               (predicate_abstract pc0 actionOf pa).  (* [sys2] *)
Proof.
  simplify.
  apply Simulates.
  + (* Every initial state of [sys1] has some matching initial state of [sys2]. *)
    simplify.
    eexists; propositional.
    cases st1. rewrite <- H1. apply PaR.
    simplify.
    equality.
  + (* Starting from a pair of related states, every step in [sys1] can be matched
     * in [sys2], to destinations that are also related. *)
    simplify.
    cases st1.
    cases st1'.
    cases st2.
    invert H1.
    eexists (p0, _).
    propositional.

    Focus 2.

    apply PredicateStep with (act := act).
    invert H0. assumption.
    apply PaR.

    simplify.
    cases (Rules pa $? act).
    - (* if action has a rule *)
      induct H1.
      eapply transition_accurate.
      unfold predicate_abstraction_sound in H.
      simplify; eapply H.
      apply Heq.
      apply H1.
    
      eapply paR_abstracts.
      apply H0.
      apply H7.
      Focus 2. apply x.
      equality.
    - (* if action does not have a rule *)
      simplify. invert H1.

Qed.
