(** * 6.887 Formal Reasoning About Programs, Spring 2016 - Pset 2 *)

Require Import Frap Pset2Sig.

Definition contrib_from (s : increment_program) :=
  match s with
  | UnsetFlag => 1
  | Done => 1
  | _ => 0
  end.

Definition has_set_flag (s : increment_program) :=
  match s with
  | SetFlag => false
  | Done => false
  | _ => true
  end.
                

Definition turn_ok (self other : increment_program) (me turn : bool) :=
  match self with
  | Read => turn = me \/ false = has_set_flag other \/ other = SetTurn
  | Write v => (turn = me \/ false = has_set_flag other \/ other = SetTurn) /\ v = contrib_from other
  | UnsetFlag => turn = me \/ false = has_set_flag other \/ other = SetTurn
  | _ => True
  end.

Inductive stronger_invariant :
  threaded_state inc_state (increment_program * increment_program) -> Prop :=
| InvCtor: forall pr1 pr2 turn,
    turn_ok pr1 pr2 false turn ->
    turn_ok pr2 pr1 true turn ->
    stronger_invariant {| Shared := {| Flag1 := has_set_flag pr1; Flag2 := has_set_flag pr2;
                                       Turn := turn;
                                       Global := contrib_from pr1 + contrib_from pr2 |};
                          Private := (pr1, pr2) |}.

(*-----------------------------------------------------*)

Lemma InvCtor' :
  forall pr1 pr2 turn sh,
    sh = {| Flag1 := has_set_flag pr1; Flag2 := has_set_flag pr2;
            Turn := turn;
            Global := contrib_from pr1 + contrib_from pr2 |} ->
    turn_ok pr1 pr2 false turn ->
    turn_ok pr2 pr1 true turn ->
    stronger_invariant {| Shared := sh ; Private := (pr1, pr2) |}.
Proof.
  intros.
  rewrite H.
  apply InvCtor; assumption.
Qed.

Theorem invariant_ok :
  invariantFor increment2_sys stronger_invariant.
Proof.
  apply invariant_induction; simplify.

  + invert H; invert H1; invert H0.
    eapply InvCtor'.
    - simplify.
      equality.
    - simplify; equality.
    - simplify; equality.

  + invert H ; invert H0.
(*-----------------------------------------------------*)
    - (* This is what happens when first thread makes a step *)
      invert H6.
      (* OMG 9 subgoals :) *)
      * eapply InvCtor'; simplify; try equality.
        cases pr2; simplify; try equality.

      * eapply InvCtor'; simplify; try equality.
        cases pr2; simplify; try equality.
      * eapply InvCtor'; simplify; try equality.
        cases pr2; simplify; try equality.

      * eapply InvCtor'; simplify; try equality.
        cases pr2; simplify; try equality.
      * eapply InvCtor'; simplify; try equality.
        cases pr2; simplify; try equality.
      * eapply InvCtor'; simplify; try equality.
        cases pr2; simplify; try equality.
      * eapply InvCtor'; simplify; try equality.
        cases pr2; simplify; try equality.
      * eapply InvCtor'; invert H1; simplify; try equality.
        cases pr2; simplify; try equality.
      * eapply InvCtor'; simplify; try equality.
        cases pr2; simplify; try equality.

    - (* now second thread makes a step *)
      invert H6.
      (* OMG 9 subgoals :) *)
      * eapply InvCtor'; simplify; try equality.
        cases pr1; simplify; try equality.

      * eapply InvCtor'; simplify; try equality.
        cases pr1; simplify; try equality.
      * eapply InvCtor'; simplify; try equality.
        cases pr1; simplify; try equality.

      * eapply InvCtor'; simplify; try equality.
        cases pr1; simplify; try equality.
      * eapply InvCtor'; simplify; try equality.
        cases pr1; simplify; try equality.
      * eapply InvCtor'; simplify; try equality.
        cases pr1; simplify; try equality.
      * (* cases pr1; simplify; try equality.
        eapply InvCtor'; simplify; try equality.
        eapply InvCtor'; simplify; try equality.
        eapply InvCtor'; simplify; try equality.
        eapply InvCtor'; simplify; try equality.
        eapply InvCtor'; simplify; try equality. *)

        eapply InvCtor'; simplify; try equality.
        cases pr1; simplify; try equality.
        (* but [contrib_from pr1 + 0] does not simplify :( *)
        rewrite Nat.add_0_r; equality.
      * eapply InvCtor'; simplify.
        (* but [contrib_from pr1 + 1] does not simplify... :( *)
        rewrite Nat.add_1_r. invert H2. equality.
        cases pr1; simplify; equality.
        cases pr1; simplify; equality.
      * eapply InvCtor'; simplify; try equality.
        cases pr1; simplify; try equality.
Qed.


        
Theorem increment2_ok :
  invariantFor increment2_sys increment2_correct.
Proof.
    apply invariant_weaken with (invariant1 := stronger_invariant).
    apply invariant_ok.

    simplify.
    invert H.
    cases pr1 ; unfold increment2_correct ; simplify ; invert H.

    simplify. equality.
Qed.

