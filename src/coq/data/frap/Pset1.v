(** * 6.887 Formal Reasoning About Programs, Spring 2016 - Pset 1 *)

Require Import Frap Pset1Sig.

(*-----------------------------------------------------*)

(*-----------------------------------------------------*)

Theorem allPositive_ok : forall (m : map_) (x : nat), allPositive m -> x > 0 -> interp_map m x > 0.
Proof.
  induction m; simplify.
  + assumption.
  + assumption.
  + apply Nat.add_pos_l.
    apply IHm1.
    apply H.
    apply H0.
  + apply IHm1.
    apply H.
    apply IHm2.
    apply H.
    apply H0.
Qed.

Theorem fold_comm : forall A (f : A -> A -> A), (forall x y, f x y = f y x) ->
                                         (forall x y z, f (f x y) z = f x (f y z)) ->
                      forall l1 l2 z, fold_left f (l1 ++ l2) z = fold_left f (l2 ++ l1) z.
Proof.
  intros A f Hsymm Hassoc.
  induct l1.
  simplify; rewrite app_nil_r; equality.
  induct l2.
  rewrite app_nil_r; equality.
  simplify. rewrite IHl1. simplify.
  rewrite <- IHl2.
  rewrite <- IHl1.

  f_equal.
  rewrite Hassoc; rewrite Hassoc. rewrite (Hsymm a).
  equality.
Qed.


Theorem reduce_swap : forall e ls1 ls2, interp_reduce e (ls1 ++ ls2) = interp_reduce e (ls2 ++ ls1).
Proof.
  induct e; simplify.
  apply fold_comm; linear_arithmetic.
  apply fold_comm; linear_arithmetic.
Qed.


Theorem interp_swap : forall e ls1 ls2,
    interp_mr e (ls1 ++ ls2) = interp_mr e (ls2 ++ ls1).
Proof.
  induct e.
  intros.
  unfold interp_mr. rewrite map_app; rewrite map_app.
  apply reduce_swap.
Qed.

Theorem fold_assoc :  forall A (f : A -> A -> A) z,
    (forall x y z, f (f x y) z = f x (f y z)) ->
    (forall x, f x z = x) -> (forall x, f z x = x) ->
    forall l1 l2 a, fold_left f (l1 ++ l2) a = f (fold_left f l1 a) (fold_left f l2 z).
Proof.
  induct l1.
  
  - induct l2.
    + simplify; equality.
    + simplify.
      rewrite IHl2.
      rewrite H.
      equality.

  - destruct l2.
    + simplify; rewrite app_nil_r; equality.
    + simplify.
      rewrite IHl1.
      equality.
Qed.

Theorem mapReduce_partition_two : forall m r ls1 ls2,
    interp_mr (m, r) (ls1 ++ ls2) =
    interp_reduce r [interp_mr (m, r) ls1; interp_mr (m, r) ls2].
Proof.
  induct r.

  unfold interp_mr; simplify.
  rewrite map_app.
  apply fold_assoc; linear_arithmetic.

  unfold interp_mr; simplify.
  rewrite map_app.
  apply fold_assoc; linear_arithmetic.
Qed.

Arguments app {_} _ _ .

Lemma fold_zero : forall A (f : A -> A -> A) z,
    (forall x y z, f (f x y) z = f x (f y z)) ->
    (forall x, f x z = x) -> (forall x, f z x = x) ->
    forall l a, fold_left f l a = f a (fold_left f l z).
Proof.
  induct l.

  simplify. rewrite H0. equality.

  simplify.
  rewrite IHl.
  rewrite H.
  equality.
Qed.

Lemma reduce_cons : forall r a ls,
    interp_reduce r (a :: ls) = interp_reduce r [a ; interp_reduce r ls].
Proof.
  induct r; simplify.

  rewrite (fold_zero _ _ 0); linear_arithmetic.
  rewrite (fold_zero _ _ 0); linear_arithmetic.
Qed.
  
  
Theorem mapReduce_partition : forall m r lsls,
    interp_mr (m, r) (fold_left app lsls []) =
    interp_reduce r (List.map (interp_mr (m, r)) lsls).
Proof.
  induct lsls.

  unfold interp_mr; simplify; equality.

  simplify.
  rewrite (fold_zero _ _ []).
  rewrite mapReduce_partition_two.
  rewrite IHlsls.

  symmetry;   apply reduce_cons.

  apply app_assoc_reverse.
  apply app_nil_r.
  simplify; equality.
Qed.
  
(* For full credit, the code you turn in can't use [Admitted] or [admit] anymore! *)
