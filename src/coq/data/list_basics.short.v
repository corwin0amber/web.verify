(* ###################################################################### *)
(** * Proofs and Programs *)

(** (We use [admit] and [Admitted] to hide solutions from exercises.) *)

Axiom admit : forall {T}, T.

(** [Inductive] is Coq's way of defining an algebraic datatype.  Its
    syntax is similar to OCaml's ([type]) or Haskell's ([data]). Here,
    we define [bool] as a simple algebraic datatype. *)

Module Bool.

Inductive bool : Type :=
| true : bool
| false : bool.

(** **** Exercise: 1 star (trivalue)  *)
(** Define a three-valued data type, representing ternary logic.  Here
    something can be true, false and unknown. *)

Inductive trivalue : Type :=
| yep : trivalue
| nope : trivalue
| dontcare : trivalue.
               
(*-----------------------------------------------------*)

(* FILL IN HERE *)

(** [] *)

Definition negb (b:bool) : bool :=
  match b with
  | true => false
  | false => true
  end.

Definition orb (b1 b2: bool) : bool :=
  match b1, b2 with
    | false, false => false
    | _, _ => true
  end.

(* WORK IN CLASS *)

(* Print orb. *)

(** We can also use an [if] statement, which matches on the first
    constructor of any two-constructor datatype, in our definition. *)

Definition andb (b1 b2: bool) : bool := if b1 then b2 else false.
(* WORK IN CLASS *)

(** Let's test our functions. The [Compute] command tells Coq to
    evaluate an expression and print the result on the screen.*)

(*Compute (negb true).
Compute (orb true false).
Compute (andb true false).
*)

(** **** Exercise: 1 star (xor)  *)
(** Define xor (exclusive or). *)

Definition xorb (b1 b2 : bool) : bool :=
  match b1 with
    | true => negb b2
    | false => b2
  end.


(* Compute (xorb true true). *)
(** [] *)


(** New tactics
    -----------

    - [intros]: Introduce variables into the context, giving them
      names.

    - [simpl]: Simplify the goal.

    - [reflexivity]: Prove that some expression [x] is equal to itself. *)

Example andb_false_l : forall b, andb false b = false.
Proof.
intros. simpl.
reflexivity.
Qed.


(** **** Exercise: 1 star (orb_true_l)  *)
Theorem orb_true_l :
  forall b, orb true b = true.
Proof.
intros b.
simpl.
reflexivity.
Qed.

(** [] *)

(** New tactic
    ----------

    - [destruct]: Consider all possible constructors of an inductive
      data type, generating subgoals that need to be solved
      separately. *)


(*  FULL: Here's an example of [destruct] in action. *)

Lemma orb_true_r : forall b : bool, orb b true = true.
(* Here we explicitly annotate b with its type, even though Coq could infer it. *)
Proof.
  intros.
  destruct b.
  + simpl. reflexivity.
  + reflexivity.
Qed.


(** We can call [destruct] as many times as we want, generating deeper subgoals. *)

Theorem andb_commutative : forall b1 b2 : bool, andb b1 b2 = andb b2 b1.
Proof.
intros.
destruct b1.
+ destruct b2.
  - simpl. reflexivity.
  - simpl. reflexivity.
+ destruct b2; reflexivity.
Qed.        
    (** **** Exercise: 1 star (andb_false_r)  *)
(** Show that b AND false is always false  *)

Theorem andb_false_r : forall b : bool, andb b false = false.
Proof.
+ destruct b; reflexivity.
Qed.
  (** [] *)

(** **** Exercise: 1 star (xorb_b_neg_b)  *)
(** Show that b xor (not b) is always true. *)

Theorem xorb_b_neg_b : forall b : bool, xorb b (negb b) = true.
Proof.
destruct b; reflexivity.
Qed.

Theorem rewrite_example : forall b1 b2 b3 b4,
  b1 = b4 ->
  b2 = b3 ->
  andb b1 b2 = andb b3 b4.

Proof.
 intros b1 b2 b3 b4 eq14 eq23.
 rewrite eq14.
 rewrite <- eq23.
 apply andb_commutative.
Qed.
(** New tactics
    -----------

    - [rewrite]: Replace one side of an equation by the other.

    - [apply]: Suppose that the current goal is [Q]. If [H : Q], then
      [apply H] solves the goal. If [H : P -> Q], then [apply H]
      replaces [Q] by [P] in the goal. If [H] has multiple hypotheses,
      [H : P1 -> P2 -> ... -> Pn -> Q], then [apply H] generates one
      subgoal for each [Pi]. *)

(* WORK IN CLASS *)


(** **** Exercise: 1 star (xorb_same)  *)
(** Show that if [b1 = b2] then b1 xor b2 is false. *)

Theorem xorb_same : forall b1 b2, b1 = b2 -> xorb b1 b2 = false.
Proof.
  intros b1 b2 eq12.
  rewrite eq12.
  destruct b2 ; reflexivity.
Qed.

End Bool.


(* ###################################################################### *)

(** We will use the following option to make polymorphism more
    convenient. *)

Set Implicit Arguments.

(** * Lists *)

Module List.

Inductive list (T : Type) :=
| nil : list T
| cons : T -> list T -> list T.

Fixpoint app T (l1 l2 : list T) : list T :=
  match l1 with
    | nil _ => l2
    | cons x xs => cons x (app xs l2)
  end.

(* WORK IN CLASS *)

Notation "h :: t" := (cons h t) (at level 60, right associativity).
Notation "[ ]" := (nil _).
Notation "[ x ; .. ; y ]" := (cons x .. (cons y [] ) ..).
Notation "l1 ++ l2" := (app l1 l2) (at level 60, right associativity).

(** Since [nil] can potentially be of any type, we add an underscore
    to tell Coq to infer the type from the context. *)

(*
Check [].
Check true :: [].
Check [true ; false].
Check [true ; false] ++ [false].

Compute [true ; false] ++ [false].
*)

Reserved Notation "l1 @ l2" (at level 60).
Fixpoint app' T (l1 l2 : list T) : list T :=
  match l1 with
  | [] => l2
  | h :: t  => h :: (t @ l2)
  end

  where "l1 @ l2" := (app' l1 l2).


(** **** Exercise: 1 star (snoc)  *)
(** Define [snoc], which adds an element to the end of a list. *)

Fixpoint snoc {T} (l : list T) (x : T) : list T :=
  match l with
    | [] => [ x ]
    | y :: ys => y :: (snoc ys x)
  end.

(*-----------------------------------------------------*)
Compute snoc [1] 2.

Lemma app_nil_l: forall T (l : list T), [] ++ l  = l.
Proof.
  intros T l.
reflexivity.
Qed.

Lemma app_nil_r: forall T (l : list T), l ++ []  = l.
Proof.
  intros T l.
  induction l as [|x xs IH].
  + reflexivity.
  + simpl.
    rewrite IH.
    reflexivity.
Qed.    
    (** New tactic
    ----------

    - [induction]: Argue by induction. It works as [destruct], but
    additionally giving us an inductive hypothesis in the inductive
    case. *)
(* WORK IN CLASS *)

Lemma app_assoc :
  forall T (l1 l2 l3 : list T),
    l1 ++ (l2 ++ l3) = (l1 ++ l2) ++ l3.
Proof.
  intros T l1 l2 l3.
  induction l1 as [|x xs IH].
  + reflexivity.
  + simpl.
    rewrite <- IH.
    reflexivity.
Qed.


    (* WORK IN CLASS *)

(** **** Exercise: 2 stars (snoc_app)  *)
(** Prove that [snoc l x] is equivalent to appending [x] to the end of
    [l]. *)

Lemma snoc_app : forall T (l : list T) (x : T), snoc l x = l ++ [x].
Proof.
  intros T l x.
  induction l as [| ? ? IH].
  + reflexivity.  
  + simpl. 
    rewrite <- IH.
    reflexivity.
Qed.
    (** [] *)

(** The natural numbers are defined in Coq's standard library as follows:

    Inductive nat : Type :=
      | O : nat
      | S : nat -> nat.

    where [S] stands for "Successor".

    Coq prints [S (S (S O))] as "3", as you might expect.

*)

(* Set Printing All. *)

Check 0.
Check 2.
Check 2 + 2.

(* Unset Printing All. *)

Check S (S (S O)).
Check 2 + 3.
Compute 2 + 3.

Fixpoint length T (l : list T) :=
  match l with
    | [] => O
    | x :: xs => S (length xs)
  end.

(* WORK IN CLASS *)

Compute length [1; 1; 1].

(** **** Exercise: 3 stars (app_length)  *)

Lemma app_length : forall T (l1 l2 : list T),
  length (l1 ++ l2) = length l1 + length l2.
Proof.
  intros T l1 l2.
  induction l1 as [|x xs IH].  
  + reflexivity.
  + simpl.
    rewrite IH.
    reflexivity.
Qed.



(** New Tactic
    ----------

    - [discriminate]: Looks for an equation between terms starting
      with different constructors, and solves the current goal. *)

Lemma app_eq_nil_l : forall T (l1 l2 : list T),
  l1 ++ l2 = [] -> l1 = [].
Proof.
  intros T l1 l2 H.
  induction l1 as [|x xs IH].
  + reflexivity.
  + simpl in H.
    discriminate.
Qed.

(** **** Exercise: 2 stars (app_eq_nil_r)  *)
(** Prove the same about [l2]. *)

Lemma plus_nil_r : forall T (l1 l2 : list T),
  l1 ++ l2 = [] -> l2 = [].
Proof.
  intros T l1 l2 H.
  destruct l1.
  + apply H.
  + discriminate H.
Qed.

  (** [] *)

Fixpoint shuffle T (l1 l2 : list T) {struct l2} :=
  match l1,l2 with
    | [], _ => l2
    | _,[]  => l1
    | h1 :: t1, h2 :: t2 => h1 :: h2 :: (shuffle t1 t2)
  end.

Print shuffle.


Fixpoint rev T (l : list T) :=
  match l with
    | [] => []
    | x :: xs => snoc (rev xs) x
  end.

(* WORK IN CLASS *)

Lemma rev_app : forall T (l1 l2 : list T), rev (l1 ++ l2) = rev l2 ++ rev l1.
Proof.
  intros.  
  induction l1 as [|x xs IH].
  + simpl.
    symmetry.
    apply app_nil_r.
  + simpl.
    rewrite IH.
    rewrite snoc_app.    
    rewrite snoc_app.
    rewrite app_assoc.
    reflexivity.
Qed.
    
    (* WORK IN CLASS *) 

(** **** Exercise: 2 stars (rev_app)  *)
(** Using [rev_app], prove that reversing a list twice results in the
    same list. *)

Lemma rev_involutive : forall T (l : list T), rev (rev l) = l.
Proof.
  intros.
  induction l as [|x xs IH].
  + reflexivity.
  + simpl.
    rewrite snoc_app.
    rewrite rev_app.
    rewrite IH.    
    reflexivity.
Qed.


Fixpoint tr_rev_aux {T} (l acc : list T) : list T :=
  match l with
  | [] => acc
  | x :: l => tr_rev_aux l (x :: acc)
  end.

Definition tr_rev {T} (l: list T) := tr_rev_aux l [].


Definition tr_rev_aux_eq_rev :
  forall T (l1 l2 : list T), tr_rev_aux l1 l2 = rev l1 ++ l2.
Proof.
  intros T l1.
  induction l1 as [|x xs IH].
  + reflexivity.
  + intro l2. simpl.
    rewrite IH.
    rewrite snoc_app.
    rewrite <- app_assoc. reflexivity.
Qed.
    (** New Tactic
    ----------

    - [unfold]: Calling [unfold foo] expands the definition of [foo]
      in the goal.
*)

Lemma tr_rev_eq_rev :
  forall T (l : list T),
    tr_rev l = rev l.
Proof.
  intros T l.
  unfold tr_rev.
  rewrite tr_rev_aux_eq_rev.
  apply app_nil_r.
Qed.

(* Locate "++". *)
    (* WORK IN CLASS *) 

End List.
(*-----------------------------------------------------*)


(** You may have noticed that several of the proofs in this section,
    particularly regarding the associativity of the append function,
    closely resemble proofs about arithmetic. You might also be interested
    in Coq for its ability to prove results in mathematics. The
    material in this section should be sufficient for you to start
    formulating theorems about the natural numbers, such as the
    commutativity, associativity and distributivity of addition and
    multiplication.

    As noted above, the natural numbers are defined as follows:

    Inductive nat : Type :=
      | O : nat
      | S : nat -> nat.

    From there you can define +, -, *, /, ^ etc. We encourage you to
    start on your own, but to help we've included a module on arithmetic
    We hope you enjoy.

*)
